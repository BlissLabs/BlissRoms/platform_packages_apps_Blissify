<?xml version="1.0" encoding="utf-8"?>
<!--
     Copyright (C) 2014-2021 The BlissRoms Project

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
-->

<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">

    <!-- Blissify -->
    <string name="blissify_dashboard_title">Blissify</string>

    <!-- Sub category -->
    <string name="dt2s_title">DT2S</string>
    <string name="power_key">Power</string>

    <!-- Lock screen general category -->
    <string name="general_category">General</string>
    <string name="lockscreen_shortcuts">Lockscreen shortcuts</string>

    <!-- Lock screen icons -->
    <string name="lockscreen_tuner_title">Lock screen shortcuts</string>
    <string name="fingerprint_category">Fingerprint</string>    

    <!-- Status bar items -->
    <string name="statusbar_items_category">Status bar items</string>

    <!-- General strings -->
    <string name="default_string">Default</string>
    <string name="search">Search</string>
    <string name="search_apps">Search apps</string>

    <!-- Categories -->
    <string name="blissify_animation_title">Animations</string>
    <string name="blissify_buttons_title">Buttons</string>
    <string name="blissify_gestures_title">Gestures</string>
    <string name="blissify_lockscreen_title">Lock screen</string>
    <string name="blissify_misc_title">Miscellaneous</string>
    <string name="blissify_navigation_title">Navigation</string>
    <string name="blissify_notifications_title">Notifications</string>
    <string name="blissify_powermenu_title">Power menu</string>
    <string name="blissify_quicksettings_title">Quick settings</string>
    <string name="blissify_status_bar_title">Status bar</string>
    <string name="blissify_themes_title">Themes</string>
    <string name="powermenu_items_title">Power menu items</string>
    <string name="battery_category">Battery styles</string>

    <!-- Notification light -->
    <string name="notification_light_settings">Notification light</string>

    <!-- Battery light -->
    <string name="light_settings_header">Light settings</string>
    <string name="battery_light_settings">Battery charging light</string>
    <string name="battery_light_summary">Customizations for battery charging light</string>
    <string name="battery_light_enable">Enable</string>
    <string name="battery_full_light_enabled">Battery light when full charged</string>
    <string name="battery_light_allow_on_dnd_title">Battery light in Do Not Disturb mode</string>
    <string name="battery_light_low_blinking_title">Blinking light on low battery</string>
    <string name="battery_light_cat">Battery light color when charging</string>
    <string name="battery_light_low_color">Low battery</string>
    <string name="battery_light_medium_color">Medium battery</string>
    <string name="battery_light_full_color">Almost full battery</string>
    <string name="battery_light_reallyfull_color">Full (100) battery</string>

    <!-- Brightness control -->
    <string name="status_bar_toggle_brightness">Brightness control</string>
    <string name="status_bar_toggle_brightness_summary">Adjust brightness by sliding across the status bar</string>

    <!-- Gaming mode -->
    <string name="gaming_mode_title">Game space</string>
    <string name="gaming_mode_summary">Extra features for immersive gaming experience</string>

    <!-- Lockscreen battery info -->
    <string name="lockscreen_charging_info_title">Show charging info in lockscreen</string>
    <string name="lockscreen_charging_info_summary">Shows temperature, charging current and more while plugged in</string>

    <!-- Heads up -->
    <string name="headsup_category">HeadsUp Settings</string>
    <string name="heads_up_notifications">Heads up</string>
    <string name="heads_up_notifications_summary">Customise heads up notifications settings</string>
    <string name="summary_heads_up_enabled">Pop-up notifications are enabled</string>
    <string name="summary_heads_up_disabled">Pop-up notifications are disabled</string>
    <string name="add_heads_up_package">Add app</string>
    <string name="add_heads_up_stoplist_summary">Disable heads up notifications in these applications</string>
    <string name="add_heads_up_blacklist_summary">Disable heads up from these applications</string>
    <string name="heads_up_stoplist_title">Stoplist</string>
    <string name="heads_up_blacklist_title">Blacklist</string>
    <string name="profile_choose_app">Choose app</string>
    <string name="dialog_delete_title">Delete</string>
    <string name="dialog_delete_message">Remove selected item?</string>

    <!-- Heads up timeout -->
    <string name="heads_up_time_out_title">Time out</string>
    <string name="heads_up_time_out_summary">Peeking notifications will show for <xliff:g id="number">%d</xliff:g> seconds</string>
    <string name="heads_up_time_out_2sec">2 seconds</string>
    <string name="heads_up_time_out_4sec">4 seconds</string>
    <string name="heads_up_time_out_5sec">5 seconds</string>
    <string name="heads_up_time_out_6sec">6 seconds</string>
    <string name="heads_up_time_out_8sec">8 seconds</string>
    <string name="heads_up_time_out_10sec">10 seconds</string>

    <!-- Heads up snooze -->
    <string name="heads_up_snooze_title">Snooze timer</string>
    <string name="heads_up_snooze_summary_one_minute">Swiping up on peeking notifications will snooze heads up from that application for 1 minute</string>
    <string name="heads_up_snooze_summary">Swiping up on peeking notifications will snooze heads up from that application for <xliff:g id="number">%d</xliff:g> minutes</string>
    <string name="heads_up_snooze_disabled_summary">Snooze timer is disabled</string>
    <string name="heads_up_snooze_1min">1 minute</string>
    <string name="heads_up_snooze_5min">5 minutes</string>
    <string name="heads_up_snooze_10min">10 minutes</string>
    <string name="heads_up_snooze_15min">15 minutes</string>
    <string name="heads_up_snooze_20min">20 minutes</string>

    <!-- Heads up options -->
    <string name="less_boring_heads_up_title">Make heads up less annoying</string>
    <string name="less_boring_heads_up_summary">Show heads up only for dialer or messaging app if they are not in foreground</string>

    <!-- Annoying Notifications -->
    <string name="notification_sound_vib_screen_on_title">Annoying Notifications</string>
    <string name="notification_sound_vib_screen_on_summary">Play sound and vibration for notifications when screen is on</string>

    <!-- Incall vibrate options -->
    <string name="incall_vibration_category">In-call vibration options</string>
    <string name="incall_vibrate_connect_title">Vibrate on connect</string>
    <string name="incall_vibrate_call_wait_title">Vibrate on call waiting</string>
    <string name="incall_vibrate_disconnect_title">Vibrate on disconnect</string>

    <!-- Double tap lock screen to sleep -->
    <string name="dt2s_lockscreen_title">DT2S on lock screen</string>
    <string name="dt2s_lockscreen_summary">Double-tap on the top third or bottom shortcut area of the lock screen to put the device to sleep</string>

    <!-- Double tap status bar to sleep -->
    <string name="double_tap_to_sleep_title">Tap on status bar</string>
    <string name="double_tap_to_sleep_summary">Double-tap status bar to put the device to sleep</string>

    <!-- Torch Power button gestures -->
    <string name="torch_power_button_gesture_title">Toggle torch when screen off</string>
    <string name="torch_power_button_gesture_none">Disabled</string>
    <string name="torch_power_button_gesture_dt">Double tap power button (slower single tap response), Disables double power tap for camera.</string>
    <string name="torch_power_button_gesture_lp">Long press power button</string>

    <!-- Volume buttons category -->
    <string name="volume_category">Volume rocker</string>
    <string name="volume_rocker_wake_title">Volume rocker wake</string>
    <string name="volume_rocker_wake_summary">Pressing the volume keys will wake your device</string>

    <!-- Partial Screenshot -->
    <string name="click_partial_screenshot_title">Click to partial screenshot</string>
    <string name="click_partial_screenshot_summary">Short click Volume Down and Power to take partial screenshot</string>

    <!-- Launch music player when headset is connected -->
    <string name="headset_connect_player_title">Launch music app on headset connection</string>
    <string name="headset_connect_player_summary">Launch the default music app when headset is connected</string>

    <!-- Network Traffic -->
    <string name="traffic_title">Traffic indicators</string>
    <string name="traffic_summary">Customize the traffic indicators</string>
    <string name="network_traffic_autohide_threshold_title">Auto-hide threshold</string>
    <string name="network_traffic_location_title">Location</string>
    <string name="network_traffic_disabled">Disabled</string>
    <string name="network_traffic_statusbar">Status bar</string>
    <string name="network_traffic_qs_header">QS header</string>
    <string name="network_traffic_refresh_interval_title">Refresh interval</string>
    <string name="network_traffic_mode_title">Display mode</string>
    <string name="network_traffic_dynamic">Dynamic</string>
    <string name="network_traffic_download">Download</string>
    <string name="network_traffic_upload">Upload</string>

    <!-- AOSP Gestures -->
    <string name="aosp_gesture_preference">System settings</string>
    <string name="aosp_gesture_summary">Stock gestures included with AOSP</string>

    <!-- Navigation -->
    <string name="system_navigation_summary">Configure navigation menus</string>

    <!-- Power Menu -->
    <string name="power_menu_title">Power menu</string>
    <string name="power_menu_title_summary">View or change power menu entries</string>
    <string name="powermenu_screenshot">Screenshot</string>
    <string name="powermenu_onthego">On-The-Go mode</string>
    <string name="powermenu_torch">Flashlight</string>
    <string name="powermenu_power">Power</string>
    <string name="powermenu_advanced">Advanced reboot options</string>
    <string name="powermenu_restart">Restart</string>
    <string name="powermenu_airplane">Airplane</string>
    <string name="powermenu_settings">Settings</string>
    <string name="powermenu_lockdown">Lock Down</string>
    <string name="powermenu_soundpanel">Sound Panel</string>
    <string name="powermenu_users">Users</string>
    <string name="powermenu_logout">Logout</string>
    <string name="powermenu_emergency">Emergency</string>

    <!-- Power menu on lockscreen -->
    <string name="lockscreen_power_menu_disabled_title">Disable power menu on lock screen</string>
    <string name="lockscreen_power_menu_disabled_summary">This will only disable the power menu on secure lock screens</string>

    <!-- Quick QS pulldown -->
    <string name="status_bar_quick_qs_pulldown">Quick QS pulldown</string>
    <string name="quick_pulldown_none">Disabled</string>
    <string name="quick_pulldown_right">Pulldown statusbar from right side</string>
    <string name="quick_pulldown_left">Pulldown statusbar from left side</string>

    <!-- Fingerprint authentication vibration -->
    <string name="fprint_sucess_vib_title">Fingerprint success vibration</string>
    <string name="fprint_sucess_vib_summary">Vibrate on successful fingerprint authentication</string>
    <string name="fprint_error_vib_title">Fingerprint error vibration</string>
    <string name="fprint_error_vib_summary">Vibrate on failed fingerprint authentication</string>

    <!-- Volume Steps Fragment -->
    <string name="volume_steps_fragment_title">Volume steps</string>
    <string name="volume_steps_summary">Customize volume steps</string>
    <string name="volume_steps_alarm_title">Volume steps: Alarm</string>
    <string name="volume_steps_dtmf_title">Volume steps: DTMF</string>
    <string name="volume_steps_music_title">Volume steps: Media</string>
    <string name="volume_steps_notification_title">Volume steps: Notification</string>
    <string name="volume_steps_ring_title">Volume steps: Ringer</string>
    <string name="volume_steps_system_title">Volume steps: System</string>
    <string name="volume_steps_voice_call_title">Volume steps: Voice Call</string>
    <string name="volume_steps_reset">Reset</string>

    <!-- Tiles on secure keyguard -->
    <string name="use_tiles_on_secure_keyguard_title">Require unlocking to use sensitive tiles</string>
    <string name="use_tiles_on_secure_keyguard_summary">Ask to unlock lockscreen when using sensitive tiles</string>

    <!-- Screen off animation -->
    <string name="screen_off_animation_title">Screen off animation</string>
    <string name="screen_off_animation_crt">CRT</string>
    <string name="screen_off_animation_scale">Scale</string>

    <!-- Sb Icon Style -->
    <string name="statusbar_icons_style">Colored Statusbar Icons</string>
    <string name="statusbar_icons_style_summary">Choose the style of your statusbar icons (requires SystemUI restart)</string>

    <!-- Notification count -->
    <string name="status_bar_notif_count_title">Show notification count</string>
    <string name="status_bar_notif_count_summary">Display the number of pending notifications</string>

    <!-- 4G icon -->
    <string name="show_fourg_icon_title">4G icon</string>
    <string name="show_fourg_icon_summary">Display 4G icon in signal icon instead LTE</string>

    <!-- Show old style mobile data icon -->
    <string name="use_old_mobiletype_title">Use old style mobile data icons</string>
    <string name="use_old_mobiletype_summary">Show mobile type icon on top of the signal indicator</string>

    <!-- Status bar battery style-->
    <string name="status_bar_battery_style_title">Battery style</string>
    <string name="status_bar_battery_style_port">Portrait</string>
    <string name="status_bar_battery_style_circle">Circle</string>
    <string name="status_bar_battery_style_dotted_circle">Dotted сircle</string>
    <string name="status_bar_battery_style_full_circle">Full Circle</string>
    <string name="status_bar_battery_style_text">Text</string>
    <string name="battery_style_category_title">Battery styles</string>

    <!-- Battery percentage -->
    <string name="status_bar_battery_percentage_title">Battery percentage</string>
    <string name="status_bar_battery_percentage_default">Hidden</string>
    <string name="status_bar_battery_percentage_text_next">Next to the icon</string>
    <string name="status_bar_battery_percentage_text_inside">Inside the icon</string>

    <!-- Show VoLTE icon on statusbar -->
    <string name="volte_switch_title">VoLTE icon</string>
    <string name="volte_switch_summary">Display VoLTE icon</string>
    <string name="volte_icon_title">VoLTE icon</string>
    <string name="volte_icon_summary">Display VoLTE icon</string>
    <string name="volte_icon_style_title">Select VoLTE Icon</string>
    <string name="volte_icon_asus">VoLTE Asus Icon</string>
    <string name="volte_icon_vo">Vo Only Icon</string>
    <string name="volte_icon_volte">VoLTE Icon</string>
    <string name="volte_icon_oos">VoLTE OOS Icon</string>
    <string name="volte_icon_hd">HD Icon</string>
    <string name="volte_icon_hd2">HD CAF Icon</string>
    <string name="volte_icon_miui">MIUI 11 Icon</string>
    <string name="volte_icon_emui">EMUI Icon</string>
    <string name="volte_icon_margaritov">HD Filled Icon</string>
    <string name="volte_icon_margaritov2">Margaritov VoLTE Icon</string>
    <string name="volte_icon_vivo">Vivo Icon</string>

    <!-- VoWiFi icon -->
    <string name="vowifi_icon_title">VoWiFi icon</string>
    <string name="vowifi_icon_enabled">VoWiFi icon enabled</string>
    <string name="vowifi_icon_disabled">VoWiFi icon disabled</string>
    <string name="vowifi_icon_enabled_volte_disabled">VoWiFi instead of VoLTE icon</string>
    <string name="vowifi_icon_style_title">VoWiFi icon style</string>
    <string name="vowifi_icon_default">Default Icon</string>
    <string name="vowifi_icon_emui">EMUI Icon</string>
    <string name="vowifi_icon_asus">Asus Icon</string>
    <string name="vowifi_icon_oneplus">OOS Icon</string>
    <string name="vowifi_icon_moto">Motorola Icon</string>
    <string name="vowifi_icon_simple1">Simple1 Icon</string>
    <string name="vowifi_icon_simple2">Simple2 Icon</string>
    <string name="vowifi_icon_simple3">Simple3 Icon</string>
    <string name="vowifi_icon_vivo">Vivo Icon</string>
    <string name="vowifi_icon_margaritov">Margaritov Icon</string>

    <!-- Data disabled icon -->
    <string name="data_disabled_icon_title">Show data disabled icon</string>
    <string name="data_disabled_icon_summary">Display the cross sign next to signal bar when data is disabled</string>

    <!-- Roaming indicator icon -->
    <string name="roaming_indicator_icon_title">Roaming indicator</string>
    <string name="roaming_indicator_icon_summary">Display roaming indicator in signal icon when in roaming mode</string>

    <!-- Double Tap on Doze to wake -->
    <string name="double_tap_on_doze_to_wake_title">Double-tap to wake on doze</string>
    <string name="double_tap_on_doze_to_wake_summary">Double tap to wake when device is dreaming</string>

    <!-- Screenshot type -->
    <string name="screenshot_type_title">Screenshot type</string>
    <string name="screenshot_type_fullscreen">Take fullscreen screenshots</string>
    <string name="screenshot_type_partial">Drag selection on the area you want to take as a screenshot</string>

    <!-- Charging animation -->
    <string name="charging_animation_title">Charging animation</string>
    <string name="charging_animation_summary">Display an animation when the device is plugged in</string>

    <!-- Notification guts kill app button -->
    <string name="notification_guts_kill_app_button_title">Kill app button</string>
    <string name="notification_guts_kill_app_button_summary">Show a kill app button in the notification\'s longpress menu</string>

    <!-- QS Battery estimates -->
    <string name="qs_show_battery_estimate_title">Battery estimates</string>
    <string name="qs_show_battery_estimate_summary_on">Show battery estimates in QS</string>
    <string name="qs_show_battery_estimate_summary_off">Hide battery estimates in QS</string>

    <!-- Fingerprint Ripple Effect -->
    <string name="enable_fingerprint_ripple_effect_title">Ripple effect</string>
    <string name="enable_fingerprint_ripple_effect_summary">Show ripple effect on unlock with fingerprint</string>

</resources>
